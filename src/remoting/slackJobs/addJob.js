import retriveData from '../global/retreiveData';
import { url, api } from '../config.json';

export default async (jobData) => {
  try {
    return await retriveData({
      url: url + api.jobs,
      method: 'POST',
      body: jobData,
    });
  } catch (error) {
    throw error;
  }
};
