import retriveData from '../global/retreiveData';
import { url, api } from '../config.json';

export default async (id) => {
  try {
    return await retriveData({
      url: `${url}${api.jobs}/${id}`,
      method: 'DELETE',
    });
  } catch (error) {
    throw error;
  }
};
