import retriveData from '../global/retreiveData';
import { url, api } from '../config.json';

export default async () => {
  try {
    return await retriveData({
      url: url + api.jobs,
      method: 'GET',
    });
  } catch (error) {
    throw error;
  }
};
