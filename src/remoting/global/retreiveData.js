const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export default async ({ url, method, body }) => {
  let myBody = { ...body };
  if (!url || !method) throw new Error('/global/retreveData -> url, method or body null !');
  try {
    if (body) myBody = { body: JSON.stringify(body) };
    const response = await fetch(url, { method, headers, ...myBody });
    if (!response.ok) throw new Error(response.statusText || response.status);
    const responseJson = await response.json();
    if (responseJson.success) return responseJson.data;
    throw new Error(responseJson.data);
  } catch (error) {
    throw error;
  }
};
