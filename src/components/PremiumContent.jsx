import React from 'react';
import { FormattedMessage } from 'react-intl';

const PremiumContent = () => (
  <FormattedMessage
    id="app.premiumContentTitle"
    defaultMessage="Premium Content"
    description="Premium Content title"
  />
);

export default PremiumContent;
