import styled, { css } from 'react-emotion';

const dropdownButtonContainerStyle = props =>
  css(`
    position: relative;
    background-color: ${props.theme.contentBackgroundColor};
    margin-right: -1px;
    width: ${props.listWidth}px; 
    display: flex;
    flex-direction: column; 
    justify-content: center;
    & .ant-dropdown-trigger {
      color: ${props.theme.contentTxtColor};
      font-size: 12px;
      text-align: center;
      text-decoration: none;
    }
    & :hover {
      background-color: ${props.theme.hoverBackgroundColor};
      cursor: pointer;
    }
    z-index: 1;
    label: footer-button-container;
  `);

export const DropdownButtonContainer = styled('div')(dropdownButtonContainerStyle);

const dropdownButtonStyle =
  css(`
    display: flex;
    flex-direction: row; 
    justify-content: center;
    label: language-dropdown-style;
  `);

export const StyledContainer = styled('div')(dropdownButtonStyle);

const menuLabelStyle = props =>
  css(`
    color: ${props.theme.footerLabelTxtColor};
    height: 29px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin-right: 2px;
    label: menu-label;    
  `);

export const MenuLabel = styled('div')(menuLabelStyle);

const subContainerStyle =
  css(`
    font-size: 12px;
    outline: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    label: sub-container;
  `);

export const SubContainer = styled('div')(subContainerStyle);

const getPosition = (opened, position, numberOfItemsInList) => {
  if (position === 'top') {
    if (opened) return `${(numberOfItemsInList * 30) - 32}px`;
    return '-32px';
  } else if (position === 'bottom') {
    if (opened) return `-${(numberOfItemsInList * 30) + 1}px`;
    return '-1px';
  }
  return false;
};
// top: ${props.opened ? `-${(props.numberOfItemsInList * 30) + 1}px` : '-1px'};
const menuContainerStyle = props =>
  css(`
    ${props.opened ? 'display: table;' : ''};
    table-layout: fixed;
    width: ${props.listWidth}px;
    position: absolute;
    top: ${getPosition(props.opened, props.position, props.numberOfItemsInList)};
    & :after {
      content: " ";
      overflow: hidden;
    }
    -webkit-transition: all 1s ease 0s;
    transition: all 1s ease 0s;
    z-index: 0;
    label: menu-container;
  `);

export const MenuContainer = styled('div')(menuContainerStyle);

const menuContainerInnerStyle = props =>
  css(`
    background-color: #ddd;
    ${props.position === 'bottom' && `border-top: 1px solid ${props.theme.borderLineColor}`};
    ${props.position === 'top' && `border-bottom: 1px solid ${props.theme.borderLineColor}`};
    border-right: 1px solid ${props.theme.borderLineColor};
    border-left: 1px solid ${props.theme.borderLineColor};
    line-height: 0;
    text-align: left;
    label: menu-container-inner;
  `);

export const MenuContainerInner = styled('div')(menuContainerInnerStyle);

export const itemStyle =
  css(`
    outline: none;
    font-size: 12px;
    color: #333;
    display: inline-table;
    vertical-align: top;
    table-layout: fixed;
    width: 100%;
    text-align: left;
    padding: 0 15px 0 11px;
    background-color: #ddd;
    border-top: 1px solid transparent;
    border-bottom: 1px solid #ccc;
    white-space: nowrap;
    line-height: 28px;
    &:hover {
      background-color: #e4e4e4;
      cursor: pointer;
    }
  `);

export const currentStateStyle =
  css(`
    height: 29px;
    display: flex;
    flex-direction: column;
    justify-content: center;
  `);

export const itemHighlitedStyle =
  css(`
    color: #14805e;
    background-color: #f0f0f0;
    position: relative;
    &:before {
      position: absolute;
      top: -1px;
      left: 0px;
      width: 4px;
      height: 30px;
      content: "";
      background-color: #4acfa5;
    }  
  `);
