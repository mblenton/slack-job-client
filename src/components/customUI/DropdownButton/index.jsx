import React from 'react';
import PropTypes from 'prop-types';
import { cx } from 'react-emotion';

import {
  DropdownButtonContainer,
  StyledContainer,
  MenuLabel,
  SubContainer,
  MenuContainer,
  MenuContainerInner,
  itemStyle,
  currentStateStyle,
  itemHighlitedStyle,
} from './style';

class DropdownButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { opened: false };
    this.handleMenuClick = this.handleMenuClick.bind(this);
    this.handleMenuItemClick = this.handleMenuItemClick.bind(this);
    this.handleAnyClick = this.handleAnyClick.bind(this);
  }

  addRemoveListener() {
    document.removeEventListener('click', this.handleAnyClick, false);
    if (!this.state.opened) {
      document.addEventListener('click', this.handleAnyClick, false);
    }
  }

  handleAnyClick() {
    if (this.state.opened) {
      this.setState({ opened: false });
    }
    this.addRemoveListener();
  }

  handleMenuClick() {
    this.addRemoveListener();
    this.setState({ opened: !this.state.opened });
  }

  handleMenuItemClick(e) {
    this.setState({ opened: !this.state.opened });
    this.props.onClickFunction({ itemkey: e.currentTarget.attributes.itemkey.value });
    this.addRemoveListener();
  }

  render() {
    const {
      // render,
      label,
      currentState,
      currentStateKey,
      items,
      listWidth,
      position,
    } = this.props;
    const { opened } = this.state;
    return (
      <div>
        <DropdownButtonContainer listWidth={listWidth}>
          <SubContainer onClick={this.handleMenuClick} role="button" tabIndex={0} onKeyUp={this.handleMenuClick}>
            <StyledContainer>
              <MenuLabel>
                {label}
              </MenuLabel>
              <div className={currentStateStyle}>{currentState}</div>
            </StyledContainer>
          </SubContainer>
        </DropdownButtonContainer>
        <MenuContainer
          numberOfItemsInList={items.length}
          listWidth={listWidth}
          opened={opened}
          position={position}
        >
          <MenuContainerInner opened={opened} position={position}>
            {items && items.map((item, index) => (
              <span
                className={
                  cx(
                    { [itemStyle]: true },
                    {
                      [itemHighlitedStyle]:
                      currentStateKey === item.key || currentState === item.key,
                    },
                  )
                }
                itemkey={item.key}
                key={item.key}
                role="button"
                tabIndex={index}
                onKeyUp={this.handleMenuItemClick}
                onClick={this.handleMenuItemClick}
              >
                {item.icon || ''}
                {item.name}
              </span>
            ))}
          </MenuContainerInner>
        </MenuContainer>
      </div>
    );
  }
}

DropdownButton.propTypes = {
  // render: PropTypes.func.isRequired,
  position: PropTypes.string.isRequired,
  currentStateKey: PropTypes.string,
  currentState: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]).isRequired,
  listWidth: PropTypes.number.isRequired,
  onClickFunction: PropTypes.func.isRequired,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]).isRequired,
};

DropdownButton.defaultProps = {
  currentStateKey: '',
};

export default DropdownButton;
