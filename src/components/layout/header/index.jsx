import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'react-emotion';

import LeftHeader from './LeftHeader';
import CenterHeader from './CenterHeader';
import RightHeader from './RightHeader';

const headerContainerStyle = props =>
  css`
    width: 100%;
    position: fixed;
    background-color: ${props.theme.contentBackgroundColor};
    top: 0px;
    grid-area: header;
    height: 29px;
    display: flex;
    flex-direction: row;
    z-index: 16;
    label: header-container;
  `;

const Container = styled('div')(headerContainerStyle);

const Header = ({ toggleSidebar }) => (
  <Container>
    <LeftHeader toggleSidebar={toggleSidebar} />
    <CenterHeader />
    <RightHeader />
  </Container>
);

Header.propTypes = {
  toggleSidebar: PropTypes.func.isRequired,
};

export default Header;
