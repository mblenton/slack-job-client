import React from 'react';
import { css } from 'emotion';
import { FormattedMessage } from 'react-intl';

const centerHeader = css({
  height: '100%',
  width: '60%',
  textAlign: 'center',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
});

const CenterHeader = () => (
  <div className={centerHeader}>
    <FormattedMessage
      id="app.title"
      defaultMessage="Welcome to {what}"
      description="Welcome header on app main page"
      values={{ what: 'react-intl' }}
    />
  </div>
);

export default CenterHeader;
