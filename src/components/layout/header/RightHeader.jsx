import React from 'react';
import styled, { css } from 'react-emotion';

import UserMenu from './UserMenu';

const rightHeaderStyle = () =>
  css`
    height: 100%;
    width: 20%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin-right: 11px;
    label: right-header;    
  `;

const Container = styled('div')(rightHeaderStyle);

const switchContainer = css(`
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
`);

const RightHeader = () => (
  <Container>
    <div className={switchContainer}><UserMenu /></div>
  </Container>
);

export default RightHeader;
