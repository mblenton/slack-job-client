import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'react-emotion';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import bars from '@fortawesome/fontawesome-free-solid/faBars';

const leftHeaderCssClass = css({
  height: '100%',
  width: '20%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
});

const iconStyle = props =>
  css(`
    color: ${props.theme.headerUserLabelTxtColor};
    font-size: 18px;
    margin-left: 7px;
    cursor: pointer;
    label: left-menu-switch-icon;
  `);

const FontAwesomeIconStyled = styled(FontAwesomeIcon)(iconStyle);

const sidebarButtonHeaderLeft =
  css`
    outline: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    label: sidebar-button-header-left;
  `;

const LeftHeader = ({ toggleSidebar }) => (
  <div className={leftHeaderCssClass}>
    <div role="button" tabIndex={0} className={sidebarButtonHeaderLeft} onClick={toggleSidebar} onKeyUp={toggleSidebar}>
      <FontAwesomeIconStyled icon={bars} />
    </div>
  </div>
);

LeftHeader.propTypes = {
  toggleSidebar: PropTypes.func.isRequired,
};

export default LeftHeader;
