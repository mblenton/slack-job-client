import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'react-emotion';
import { FormattedMessage } from 'react-intl';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import chewronDownSolid from '@fortawesome/fontawesome-free-solid/faChevronDown';
import signInSolid from '@fortawesome/fontawesome-free-solid/faSignInAlt';

import DropdownButton from '../../customUI/DropdownButton';

const userName = 'Pero Perić';

const iconStyle = props =>
  css(`
    color: ${props.theme.headerUserLabelTxtColor};
    font-size: 12px;
    margin-top: 4px;
    margin-left: 36px;
    label: sound-switch-icon;
  `);

const FontAwesomeIconStyled = styled(FontAwesomeIcon)(iconStyle);

const logInMessage = (
  <FormattedMessage
    id="header.login"
    defaultMessage="Login"
    description="Login label on header"
  />
);

const changePassword = (
  <FormattedMessage
    id="header.changePassword"
    defaultMessage="Change password"
    description="Change password txt on right header menu"
  />
);

const logOut = (
  <FormattedMessage
    id="header.logOut"
    defaultMessage="Logout"
    description="Logout txt on right header menu"
  />
);

const currentState = userState => (
  <div><FontAwesomeIconStyled icon={!userState ? chewronDownSolid : signInSolid} /></div>
);

const UserMenu = ({ userLoginLogout, userState }) => (
  <DropdownButton
    position="top"
    listWidth={120}
    items={[{ name: changePassword, key: 'changepassword' }, { name: logOut, key: 'logout' }]}
    label={userName || logInMessage}
    currentState={currentState(userState)}
    onClickFunction={userLoginLogout}
  />
);

UserMenu.propTypes = {
  userState: PropTypes.string,
  userLoginLogout: PropTypes.func,
};

const tmpFunc = () => ('a');

UserMenu.defaultProps = {
  userState: '',
  userLoginLogout: tmpFunc,
};

export default UserMenu;

