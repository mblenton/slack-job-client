import React from 'react';
import { css } from 'emotion';
import { ThemeProvider } from 'emotion-theming';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { IntlProvider, addLocaleData } from 'react-intl';

import { translatedMessages, localeData } from '../../translations/config';
import { themeStyles } from '../../theme/config';
import Header from '../layout/header';
import Sidebar from '../layout/sidebar';
import Content from '../layout/content';
import Footer from '../layout/footer';
import { sideMenuWidth } from '../../config.json';

addLocaleData(localeData);

const wrapper = css(`
  display: grid;
  grid-gap: 0px;
  height: 100vh;
  grid-template-columns: auto 1fr;
  grid-template-rows: auto auto auto;
  grid-template-areas: "header header" "sidebar content" "footer footer";
  label: layout;
`);

class Layout extends React.Component {
  constructor(props) {
    super(props);
    this.state = { contentMarginLeft: '30px' };
    this.toggleSidebar = this.toggleSidebar.bind(this);
  }
  /* eslint-disable no-console */
  componentDidUpdate() {
    console.log('componentDidUpdate - layout');
  }

  toggleSidebar() {
    this.setState((prevState) => {
      if (prevState && prevState.contentMarginLeft === '30px') {
        // opened
        return {
          contentMarginLeft: sideMenuWidth,
        };
      }
      return {
        contentMarginLeft: '30px',
      };
    });
  }

  render() {
    const {
      contentMarginLeft,
    } = this.state;
    const {
      theme,
      language,
    } = this.props;

    return (
      <ThemeProvider theme={themeStyles[theme]}>
        <IntlProvider locale={language} messages={translatedMessages[language]}>
          <div className={wrapper}>
            <Header toggleSidebar={this.toggleSidebar} />
            <Sidebar toggleSidebar={this.toggleSidebar} />
            <Content contentMarginLeft={contentMarginLeft} />
            <Footer />
          </div>
        </IntlProvider>
      </ThemeProvider>
    );
  }
}

Layout.propTypes = {
  // location: PropTypes.shape(PropTypes.object).isRequired,
  language: PropTypes.string.isRequired,
  theme: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  theme: state.layoutReducer.theme,
  language: state.layoutReducer.language,
});

export default connect(mapStateToProps, null)(Layout);
