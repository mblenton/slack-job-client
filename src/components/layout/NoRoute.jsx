import React from 'react';

export default () => (
  <div>
    <h1>404</h1>
    <h4>Not found</h4>
  </div>
);
