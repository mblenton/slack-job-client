import React from 'react';
import PropTypes from 'prop-types';
import { css } from 'emotion';
import { Icon } from 'antd';
import { FormattedMessage } from 'react-intl';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import DropdownButton from '../../customUI/DropdownButton';
import changeSoundAction from '../../../redux/actions/layout/changeSound';

const iconStyle = css(`
    font-size: 16px;
    width: 25px;
    height: 29px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    label: sound-switch-icon;
  `);

const formattedMessage = (
  <FormattedMessage
    id="footer.sound"
    defaultMessage="Sound"
    description="Sound label on footer"
  />
);

const currentState = soundState => (
  <div className={iconStyle}><Icon type={soundState ? 'sound' : 'sound'} /></div>
);

/* eslint-disable no-shadow */
const SoundSwitch = ({ changeSoundAction, soundState }) => (
  <DropdownButton
    position="bottom"
    listWidth={90}
    items={[{ name: 'On', key: 'on' }, { name: 'Off', key: 'off' }]}
    label={formattedMessage}
    currentState={currentState(soundState)}
    onClickFunction={changeSoundAction}
  />
);
/* eslint-disable no-shadow */

SoundSwitch.propTypes = {
  soundState: PropTypes.string.isRequired,
  changeSoundAction: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => bindActionCreators({ changeSoundAction }, dispatch);

const mapStateToProps = state => ({
  language: state.layoutReducer.language,
  soundState: state.layoutReducer.soundState,
  error: state.layoutReducer.error,
});

export default connect(mapStateToProps, mapDispatchToProps)(SoundSwitch);
