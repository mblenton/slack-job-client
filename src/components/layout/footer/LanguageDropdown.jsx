import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import translations from '../../../translations/config';
import DropdownButton from '../../customUI/DropdownButton';
import changeLanguageAction from '../../../redux/actions/layout/changeLanguage';

const formattedMessage = (
  <FormattedMessage
    id="footer.language"
    defaultMessage="Language"
    description="Language label on footer"
  />
);

/* eslint-disable no-shadow */
const LanguageDropdown = ({ language, changeLanguageAction }) => (
  <DropdownButton
    position="bottom"
    listWidth={90}
    items={translations}
    label={formattedMessage}
    currentState={language}
    onClickFunction={changeLanguageAction}
  />
);
/* eslint-disable no-shadow */

LanguageDropdown.propTypes = {
  language: PropTypes.string.isRequired,
  changeLanguageAction: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => bindActionCreators({ changeLanguageAction }, dispatch);

const mapStateToProps = state => ({
  language: state.layoutReducer.language,
  error: state.layoutReducer.error,
});

export default connect(mapStateToProps, mapDispatchToProps)(LanguageDropdown);
