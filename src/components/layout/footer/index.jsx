import React from 'react';
import styled, { css } from 'react-emotion';

import LanguageDropdown from './LanguageDropdown';
import ThemeSwitch from './ThemeSwitch';
import SoundSwitch from './SoundSwitch';

const footerStyle = props =>
  css`
    z-index: 20;
    grid-area: footer;
    position: fixed;
    bottom: 0;
    height: 29px;
    width: 100%;
    background-color: ${props.theme.contentBackgroundColor};
    display: flex;
    flex-direction: column;
    justify-content: center;
    label: footer-style;
  `;

const Container = styled('div')(footerStyle);

const itemsContainer = css(`
  font-size: 10px;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  margin-right: 11px;
  label: items-container;
`);

const Footer = () => (
  <Container>
    <div className={itemsContainer}>
      <SoundSwitch />
      <LanguageDropdown />
      <ThemeSwitch />
    </div>
  </Container>
);

export default Footer;
