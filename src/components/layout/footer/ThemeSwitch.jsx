import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';

import themes, { themeLanguages } from '../../../theme/config';
import DropdownButton from '../../customUI/DropdownButton';
import changeThemeAction from '../../../redux/actions/layout/changeTheme';

const formattedMessage = (
  <FormattedMessage
    id="footer.theme.label"
    defaultMessage="Theme:"
    description="Theme switch label on footer"
  />
);

/* eslint-disable no-shadow */
const ThemeSwitch = ({ theme, changeThemeAction }) => (
  <DropdownButton
    position="bottom"
    listWidth={120}
    items={themes}
    label={formattedMessage}
    currentState={themeLanguages[theme]}
    currentStateKey={theme}
    onClickFunction={changeThemeAction}
  />
);
/* eslint-disable no-shadow */

ThemeSwitch.propTypes = {
  theme: PropTypes.string.isRequired,
  changeThemeAction: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => bindActionCreators({ changeThemeAction }, dispatch);

const mapStateToProps = state => ({
  language: state.layoutReducer.language,
  theme: state.layoutReducer.theme,
  error: state.layoutReducer.error,
});

export default connect(mapStateToProps, mapDispatchToProps)(ThemeSwitch);
