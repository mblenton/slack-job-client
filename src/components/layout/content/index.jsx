import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'react-emotion';

import ContentRoutes from '../../../routes/ContentRoutes';

const contentContainerStyle = props =>
  css`
    color: ${props.theme.contentTxtColor};
    position: fixed;
    top: 29px;
    grid-area: content;
    background-color: ${props.theme.contentBackgroundColor};
    border-left: 1px solid ${props.theme.borderLineColor};
    border-bottom: 1px solid ${props.theme.borderLineColor};
    height: calc(100% - 58px);
    width: 100%;
    margin-left: ${props.marginLeft};
    -webkit-transition: margin 600ms ease-in;
    transition: margin 600ms ease-in;
    z-index: 15;
    label: content-container;
  `;

const Container = styled('div')(contentContainerStyle);

const contentStyle = props =>
  css`
    width: calc(100% - ${props.marginLeft});
    border-top: 1px solid ${props.theme.borderLineColor};
    height: 100%;
    overflow-y: auto;
    position: absolute;
    -webkit-transition: width 600ms ease-in;
    transition: width 600ms ease-in;
    ::-webkit-scrollbar {
      width: 10px;
    }
    ::-webkit-scrollbar-track {
      background: #f1f1f1;
    }
    ::-webkit-scrollbar-thumb {
      background: #888;
    }
    ::-webkit-scrollbar-thumb:hover {
      background: #555;
    }
  `;

const ContentContainer = styled('div')(contentStyle);

const Content = ({ contentMarginLeft }) => (
  <Container marginLeft={contentMarginLeft}>
    <ContentContainer marginLeft={contentMarginLeft}>
      <ContentRoutes />
    </ContentContainer>
  </Container>
);

Content.propTypes = {
  contentMarginLeft: PropTypes.string.isRequired,
};

export default Content;
