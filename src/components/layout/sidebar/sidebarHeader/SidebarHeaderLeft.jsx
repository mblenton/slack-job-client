import React from 'react';
import { css } from 'react-emotion';

const sidebarHeaderLeft =
  css`
    width: 80%;
    label: sidebar-header-left;
  `;

const SidebarHeaderLeft = () => (
  <div className={sidebarHeaderLeft} />
);

export default SidebarHeaderLeft;
