import React from 'react';
import { css } from 'emotion';

import Left from './SidebarHeaderLeft';
import Right from './SidebarHeaderRight';

const sidebarHeader = css(`
  height: 25px;
  display: flex;
  flex-direction: row;
  width: 100%;
  label: sidebar-header;
`);

const SidebarHeader = () => (
  <div className={sidebarHeader}>
    <Left />
    <Right />
  </div>
);

export default SidebarHeader;
