import React from 'react';
import { css } from 'emotion';

const sidebarHeaderRight = css({
  width: '20%',
  label: 'sidebar-header-right',
});

const SidebarHeaderRight = () => <div className={sidebarHeaderRight} />;

export default SidebarHeaderRight;
