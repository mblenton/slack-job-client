import React from 'react';
import styled, { css } from 'react-emotion';

import Header from './sidebarHeader';
import Content from './sidebarContent';
import Footer from './sidebarFooter';
import { sideMenuWidth } from '../../../config.json';

const sidebarContainerStyle = props =>
  css(`
    position: fixed;
    top: 28px;
    height: calc(100% - 56px);
    grid-area: sidebar;
    background-color: ${props.theme.contentBackgroundColor};
    width: ${sideMenuWidth};
    z-index: 11;
    label: sidebar-container;
  `);

const SidebarContainer = styled('div')(sidebarContainerStyle);

const Sidebar = () => (
  <SidebarContainer>
    <Header />
    <Content />
    <Footer />
  </SidebarContainer>
);

export default Sidebar;
