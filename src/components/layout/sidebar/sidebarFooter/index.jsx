import React from 'react';
import { css } from 'emotion';

const sidebarFooter = css({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  height: 25,
  textAlign: 'center',
});

const SidebarFooter = () => <div className={sidebarFooter} />;

export default SidebarFooter;
