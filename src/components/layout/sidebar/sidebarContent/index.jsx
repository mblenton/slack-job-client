import React from 'react';
import { css } from 'emotion';
import SiderMenu from './SiderMenu';
// import SiderMenu from '../../header/RightHeader';

const sidebarContent = css({
  height: 'calc(100% - 50px)',
});

const SidebarContent = () => (
  <div className={sidebarContent}>
    <SiderMenu />
  </div>
);

export default SidebarContent;
