import React from 'react';
import { Menu, Icon } from 'antd';
import styled, { css } from 'react-emotion';
import RLink from 'react-router-dom/Link';

const siderMenuStyle = css(`
    & .ant-menu-submenu-title {
      padding-left: 7px !important;
    }
    width: 210px !important;
    label: side-menu;    
  `);

const StyledMenu = styled(Menu)(siderMenuStyle);

class SiderMenu extends React.Component {
  constructor(props) {
    super(props);
    this.rootSubmenuKeys = ['sub1', 'sub2', 'sub4'];
    this.state = { openKeys: ['sub1'] };
    this.onOpenChange = this.onOpenChange.bind(this);
  }

  onOpenChange(openKeys) {
    const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  }

  render() {
    return (
      <StyledMenu
        mode="inline"
        openKeys={this.state.openKeys}
        onOpenChange={this.onOpenChange}
        style={{ width: 256 }}
      >
        <Menu.SubMenu
          key="sub1"
          title={
            <span>
              <Icon type="mail" />
              <span>Infobip zadatak</span>
            </span>
          }
        >
          <Menu.Item key="0">
            <RLink to="/slackjobs">Slack Jobs</RLink>
          </Menu.Item>
          <Menu.Item key="1">
            <RLink to="/premium">Premium Content</RLink>
          </Menu.Item>
          <Menu.Item key="2">
            <RLink to="/">Home</RLink>
          </Menu.Item>
        </Menu.SubMenu>
        <Menu.SubMenu
          key="sub2"
          title={
            <span>
              <Icon type="appstore" />
              <span>Navigation Two</span>
            </span>
          }
        >
          <Menu.Item key="5">Option 5</Menu.Item>
          <Menu.Item key="6">Option 6</Menu.Item>
          <Menu.SubMenu key="sub3" title="Submenu">
            <Menu.Item key="7">Option 7</Menu.Item>
            <Menu.Item key="8">Option 8</Menu.Item>
          </Menu.SubMenu>
        </Menu.SubMenu>
        <Menu.SubMenu
          key="sub4"
          title={
            <span>
              <Icon type="setting" />
              <span>Navigation Three</span>
            </span>
          }
        >
          <Menu.Item key="9">Option 9</Menu.Item>
          <Menu.Item key="10">Option 10</Menu.Item>
          <Menu.Item key="11">Option 11</Menu.Item>
          <Menu.Item key="12">Option 12</Menu.Item>
        </Menu.SubMenu>
      </StyledMenu>
    );
  }
}

export default SiderMenu;
