import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';

import logout from '../../redux/actions/auth/logout';

class Logout extends Component {
  componentDidMount() {
    this.props.logout();
    localStorage.removeItem('token');
    // this.props.history.push('/login');
  }

  // componentWillReceiveProps (newProps) {
  //   console.log(newProps);
  //   if (newProps) this.props.history.push('/login');
  // }

  render() {
    return <Redirect to="/login" />;
    // return null;
  }
}

Logout.propTypes = {
  logout: PropTypes.func.isRequired,
  // history: PropTypes.shape({ push: PropTypes.func }),
};

const mapDispatchToProps = dispatch => bindActionCreators({ logout }, dispatch);
const mapStateToProps = state => ({
  logoutSucess: state.authReducer.logoutSucess,
  logoutError: state.authReducer.error,
});

export default connect(mapStateToProps, mapDispatchToProps)(Logout);
