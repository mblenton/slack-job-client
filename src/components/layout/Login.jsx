import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
// import { Button, Form, Grid, Header, Message, Segment } from 'semantic-ui-react';

import authorize from '../../redux/actions/auth/authorize';
import getSessionToken from '../../global/getSessionToken';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: '',
      errorMessage: '',
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.sessionToken = getSessionToken().sessionToken;
  }

  componentWillReceiveProps(newProps) {
    this.sessionToken = getSessionToken().sessionToken;
    if (newProps.error !== this.props.error) {
      this.setState({ errorMessage: `Error: ${newProps.error.message}` });
    }
  }

  onSubmit() {
    if (!this.state.login || !this.state.password) {
      this.setState({ errorMessage: 'Please enter username and password.' });
    } else {
      this.props.authorize({ username: this.state.login, password: this.state.password });
    }
  }

  handleInputChange(event) {
    const { target } = event;
    const { name, value } = target;
    this.setState({ [name]: value });
  }

  render() {
    const { login, password, errorMessage } = this.state;
    // const { tokenFromServer } = this.props;

    if (this.sessionToken) {
      return <Redirect to="/customers" />;
    }

    return <div className="login-form">Login: {login} {password} {errorMessage}</div>;

    // return (
    //   <div className="login-form">
    //     {/*
    //   Heads up! The styles below are necessary for the correct render of this example.
    //   You can do same with CSS, the main idea is that all the elements up to the `Grid`
    //   below must have a height of 100%.
    // */}
    //     <style>
    //       {`
    //       body > div,
    //       body > div > div,
    //       body > div > div > div.login-form {
    //         height: 100%;
    //       }
    //     `}
    //     </style>
    //     <Grid textAlign="center" style={{ height: '100%' }} verticalAlign="middle">
    //       <Grid.Column style={{ maxWidth: 450 }}>
    //         <Header as="h2" textAlign="center">
    //           Log-in to your account
    //         </Header>
    //         <Form size="large">
    //           <Segment>
    //             <Form.Input
    //               value={login}
    //               onChange={this.handleInputChange}
    //               name="login"
    //               fluid
    //               icon="user"
    //               iconPosition="left"
    //               placeholder="Username"
    //             />
    //             <Form.Input
    //               value={password}
    //               onChange={this.handleInputChange}
    //               name="password"
    //               fluid
    //               icon="lock"
    //               iconPosition="left"
    //               placeholder="Password"
    //               type="password"
    //             />

    // <Button style={{ backgroundColor: '#b8e986' }} onClick={this.onSubmit} fluid size="large">
    //               Login
    //             </Button>
    //           </Segment>
    //         </Form>
    //         {errorMessage && (
    //           <Message>
    //             <div style={{ color: 'red' }}>{errorMessage}</div>
    //           </Message>
    //         )}
    //       </Grid.Column>
    //     </Grid>
    //   </div>
    // );
  }
}

Login.propTypes = {
  authorize: PropTypes.func.isRequired,
  error: PropTypes.shape(PropTypes.object),
  // tokenFromServer: PropTypes.string,
};

Login.defaultProps = {
  error: {},
};

const mapDispatchToProps = dispatch => bindActionCreators({ authorize }, dispatch);
const mapStateToProps = state => ({
  tokenFromServer: state.authReducer.tokenFromServer,
  error: state.authReducer.error,
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
