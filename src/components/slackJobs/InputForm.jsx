import React from 'react';
import PropTypes from 'prop-types';
import { Form, Icon, Input, Button, DatePicker, TimePicker } from 'antd';
import { css } from 'react-emotion';
import moment from 'moment';

import { CLIENT_DATE_FORMAT, CLIENT_TIME_FORMAT } from '../../global/config.json';

const inputFormMessage =
  css(`
    width: 300px;
  `);

const inputFormDatePicker =
  css(`
    width: 150px;
  `);

const inputFormTimePicker =
  css(`
    width: 150px;
  `);

const FormItem = Form.Item;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}
/* eslint-disable no-console */
function onChange(time, timeString) {
  console.log(time, timeString);
}

function onOk(value) {
  console.log('onOk: ', value);
}

class InputForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleOpenChange = (open) => {
    this.setState({ open });
  }

  handleClose = () => this.setState({ open: false });

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const date = moment(values.datePicker).format('YYYY-MM-DD');
        const time = moment(values.timePicker).format('HH:mm:ss.SSS');
        const pickedDate = moment(`${date}T${time}Z`).add(-4, 'hours').format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
        this.props.sendData({
          message: values.message,
          channel: '#general',
          status: 'scheduled',
          time: pickedDate,
        });
      }
    });
    this.props.form.resetFields();
  }

  render() {
    const {
      getFieldDecorator,
      getFieldsError,
      getFieldError,
      isFieldTouched,
    } = this.props.form;

    // Only show error after a field is touched.
    const messageError = isFieldTouched('message') && getFieldError('message');
    const datePickerError = isFieldTouched('datePicker') && getFieldError('datePicker');
    const timePickerError = isFieldTouched('timePicker') && getFieldError('timePicker');
    return (
      <Form layout="inline" onSubmit={this.handleSubmit}>
        <FormItem
          validateStatus={messageError ? 'error' : ''}
          help={messageError || ''}
        >
          {getFieldDecorator('message', {
            rules: [{ required: true, message: 'Please input slack message!' }],
          })(<Input
            className={inputFormMessage}
            prefix={<Icon type="message" style={{ color: 'rgba(0,0,0,.25)' }} />}
            placeholder="Write message"
          />)}
        </FormItem>
        <FormItem
          validateStatus={datePickerError ? 'error' : ''}
          help={datePickerError || ''}
        >
          {getFieldDecorator('datePicker', {
            rules: [{ required: true, message: 'Please select date!' }],
          })(<DatePicker
            className={inputFormDatePicker}
            format={CLIENT_DATE_FORMAT}
            onChange={onChange}
            onOk={onOk}
          />)}
        </FormItem>
        <FormItem
          validateStatus={timePickerError ? 'error' : ''}
          help={timePickerError || ''}
        >
          {getFieldDecorator('timePicker', {
            rules: [{ required: true, message: 'Please select time!', type: 'object' }],
          })(<TimePicker
            className={inputFormTimePicker}
            format={CLIENT_TIME_FORMAT}
            // initialValue={moment('00:00:00', 'HH:mm:ss A')}
            onChange={onChange}
            open={this.state.open}
            onOpenChange={this.handleOpenChange}
            addon={() => (
              <Button size="small" type="primary" onClick={this.handleClose}>
                Ok
              </Button>
            )}
          />)}
        </FormItem>
        <FormItem>
          <Button
            type="primary"
            htmlType="submit"
            disabled={hasErrors(getFieldsError())}
          >
            Add job
          </Button>
        </FormItem>
      </Form>
    );
  }
}

InputForm.propTypes = {
  form: PropTypes.shape({
    getFieldDecorator: PropTypes.func,
    getFieldsError: PropTypes.func,
    getFieldError: PropTypes.func,
    isFieldTouched: PropTypes.func,
    validateFields: PropTypes.func,
    resetFields: PropTypes.func,
  }),
  sendData: PropTypes.func.isRequired,
};

InputForm.defaultProps = {
  form: {},
};

export default Form.create()(InputForm);
