import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table, Divider, Popconfirm, message } from 'antd';
import moment from 'moment';
import { css } from 'react-emotion';
// import RLink from 'react-router-dom/Link';
import { CLIENT_JOBS_DATE_FORMAT } from '../../global/config.json';
import InputForm from './InputForm';
import getJobsAction from '../../redux/actions/slackJobs/getJobs';
import addJobAction from '../../redux/actions/slackJobs/addJob';
import deleteJobAction from '../../redux/actions/slackJobs/deleteJob';

const inputFormWrapperStyle =
  css(`
    height: 110px;
    display: flex;
    flex-direction: column;
    align-items: center;
  `);

const tableStyle =
  css(`
    width: 800px;
    label: table-jobs;
  `);

const dividerStyle =
  css(`
    width: 70%;
    label: divider;
  `);

const contentStyle =
  css(`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
  `);

class SlackJobs extends React.Component {
  constructor(props) {
    super(props);
    /* eslint-disable jsx-a11y/anchor-is-valid */
    this.columns = [{
      title: 'Message',
      dataIndex: 'message',
      key: 'message',
    }, {
      title: 'Channel',
      dataIndex: 'channel',
      key: 'channel',
      align: 'center',
    }, {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      align: 'center',
    }, {
      title: 'Scheduled time',
      dataIndex: 'time',
      key: 'time',
      align: 'center',
      render: (text, record) => (
        moment(record.time).format(CLIENT_JOBS_DATE_FORMAT)
      ),
    }, {
      title: 'Delete',
      key: 'delete',
      align: 'center',
      render: (text, record) => (
        <Popconfirm title="Sure to delete?" onConfirm={() => this.deleteJob(record.$loki)}>
          <a href="#">Delete</a>
        </Popconfirm>
      ),
    }];
    this.state = {
      loading: true,
    };
    this.sendData = this.sendData.bind(this);
    this.deleteJob = this.deleteJob.bind(this);
  }

  /* eslint-disable no-console */
  /* eslint-disable react/no-did-mount-set-state */
  componentDidMount() {
    console.log('componentDidMount');
    this.props.getJobsAction();
    this.setState({ loading: false });
  }

  shouldComponentUpdate(nextProps) {
    if (this.props.jobs !== nextProps.jobs) {
      return true;
    }
    if (this.props.error.length || nextProps.error.length) {
      message.error(this.props.error || nextProps.error);
    }
    return false;
  }

  componentDidUpdate() {
    /* eslint-disable react/no-did-update-set-state */
    console.log('componentDidUpdate');
  }
  /* eslint-disable class-methods-use-this */
  deleteJob(id) {
    this.props.deleteJobAction(id);
  }

  sendData(data) {
    this.props.addJobAction(data);
  }


  render() {
    console.log('Render!');
    const { loading } = this.state;
    const { jobs } = this.props;
    return (
      <div className={contentStyle}>
        <div className={inputFormWrapperStyle}>
          <Divider className={dividerStyle}>Add new job</Divider>
          <InputForm sendData={this.sendData} />
        </div>
        <Divider className={dividerStyle}>Scheduled jobs</Divider>
        <Table
          loading={loading}
          bordered
          size="middle"
          className={tableStyle}
          columns={this.columns}
          dataSource={jobs.reverse()}
          rowKey="$loki"
        />
      </div>
    );
  }
}

SlackJobs.propTypes = {
  jobs: PropTypes.arrayOf(PropTypes.shape({
    message: PropTypes.string,
    time: PropTypes.string,
    channel: PropTypes.string,
    status: PropTypes.string,
    meta: PropTypes.object,
    $loki: PropTypes.number,
  })),
  error: PropTypes.string,
  getJobsAction: PropTypes.func.isRequired,
  addJobAction: PropTypes.func.isRequired,
  deleteJobAction: PropTypes.func.isRequired,
};

SlackJobs.defaultProps = {
  jobs: [],
  error: '',
};

const mapDispatchToProps = dispatch => bindActionCreators({
  getJobsAction,
  addJobAction,
  deleteJobAction,
}, dispatch);

const mapStateToProps = state => ({
  jobs: state.slackJobsReducer.jobs,
  error: state.slackJobsReducer.errorMessage,
});

export default connect(mapStateToProps, mapDispatchToProps)(SlackJobs);
