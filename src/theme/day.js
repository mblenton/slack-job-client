const theme = {
  day: {
    contentBackgroundColor: '#f3f4fb',
    hoverBackgroundColor: '#c6cbec',
    contentTxtColor: '#3f51b5',
    headerUserLabelTxtColor: '#000',
    footerLabelTxtColor: '#8e99d7',
    borderLineColor: '#3f51b5',
  },
};
export default theme.day;
