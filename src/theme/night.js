const theme = {
  night: {
    contentBackgroundColor: '#20295b',
    hoverBackgroundColor: '#4255bd',
    contentTxtColor: '#f3f4fb',
    headerUserLabelTxtColor: '#fff',
    footerLabelTxtColor: '#8c96d9',
    borderLineColor: '#f3f4fb',
    color: 'red',
  },
};
export default theme.night;
