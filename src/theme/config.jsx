import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled, { css } from 'react-emotion';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import lightbulbSolid from '@fortawesome/fontawesome-free-solid/faLightbulb';
import lightbulbRegular from '@fortawesome/fontawesome-free-regular/faLightbulb';

import nightTheme from './night';
import dayTheme from './day';

const listIconStyle = props =>
  css`
    color: ${props.theme.footerLabelTxtColor};
    margin-right: 8px;    
  `;
const FontAwesomeIconStyled = styled(FontAwesomeIcon)(listIconStyle);

const day = (
  <FormattedMessage
    id="footer.theme.option.day"
    defaultMessage="Day"
    description="Theme switch option on footer"
  />
);

const night = (
  <FormattedMessage
    id="footer.theme.option.night"
    defaultMessage="Night"
    description="Theme switch option on footer"
  />
);

const tmp = {
  themes: [
    {
      name: day,
      key: 'day',
      dayTheme,
      icon: <FontAwesomeIconStyled icon={lightbulbRegular} />,
    },
    {
      name: night,
      key: 'night',
      nightTheme,
      icon: <FontAwesomeIconStyled icon={lightbulbSolid} />,
    },
  ],
};

export const themeStyles = {
  day: dayTheme,
  night: nightTheme,
};

export const themeLanguages = {
  day,
  night,
};

export default tmp.themes;
