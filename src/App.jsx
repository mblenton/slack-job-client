import React from 'react';
import { connect, Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { withRouter } from 'react-router-dom';
import 'antd/dist/antd.less'; // import official less entry file

import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './redux/store';

import MainRoutes from './routes/MainRoutes';

const AppContainer = connect(state => ({ location: state.location }))(MainRoutes);

const MyApp = withRouter(connect(state => ({ location: state.location }))(AppContainer));

export const history = createBrowserHistory();

const App = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <PersistGate loading={null} persistor={persistor}>
        <MyApp />
      </PersistGate>
    </ConnectedRouter>
  </Provider>
);

export default App;
