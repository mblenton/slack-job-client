import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
/**
 * Higher-order component (HOC) to wrap restricted pages
 */

const BaseComponent = (WrappedComponent) => {
  class Restricted extends Component {
    componentWillMount() {
      this.checkAuthentication();
    }

    // componentWillReceiveProps(nextProps) {
    //   if (nextProps.location !== this.props.location) {
    //     this.checkAuthentication(nextProps);
    //   }
    // }

    checkAuthentication() {
      this.autenthicated = true; // TODO check authentication
      const { history } = this.props;
      if (!this.autenthicated) {
        history.replace({ pathname: '/login' });
      }
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  Restricted.propTypes = {
    history: PropTypes.shape({ replace: PropTypes.func }).isRequired,
    // location: PropTypes.shape(PropTypes.object).isRequired,
  };

  return withRouter(Restricted);
};

export default BaseComponent;
