import { combineReducers } from 'redux';
import slackJobsReducer from './slackJobs/slackJobsReducer';
import layoutReducer from './layout/layoutReducer';


export default combineReducers({
  slackJobsReducer,
  layoutReducer,
});
