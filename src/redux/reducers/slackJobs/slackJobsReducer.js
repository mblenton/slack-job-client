import * as types from '../../actions/slackJobs/actionTypes';

const initialState = { jobs: [], errorMessage: '' };

const slackJobsReducer = (state = initialState, { type, payload, error }) => {
  switch (type) {
    case types.GET_JOBS_SUCCESS: {
      return { ...state, jobs: payload, errorMessage: '' };
    }
    case types.GET_JOBS_FAILURE: {
      return { ...state, errorMessage: error };
    }
    case types.ADD_JOB_SUCCESS: {
      return { ...state, jobs: [...state.jobs.reverse(), payload], errorMessage: '' };
    }
    case types.ADD_JOB_FAILURE: {
      return { ...state, errorMessage: error };
    }
    case types.DELETE_JOB_SUCCESS: {
      return {
        ...state,
        jobs: state.jobs.filter(item => item.$loki !== Number(payload)).reverse(),
        errorMessage: '',
      };
    }
    case types.DELETE_JOB_FAILURE: {
      return { ...state, errorMessage: error };
    }
    default:
      return state;
  }
};

export default slackJobsReducer;
