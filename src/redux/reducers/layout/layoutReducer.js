import * as types from '../../actions/layout/actionTypes';
import { languageFromBrowserWithoutRegionCode } from '../../../translations/config';
import themes from '../../../theme/config';

const initialState = {
  theme: themes[0].key,
  language: languageFromBrowserWithoutRegionCode,
  soundState: 'On',
};

const layoutReducer = (state = initialState, { type, payload, error }) => {
  switch (type) {
    case types.CHANGE_THEME_SUCCESS: {
      return { ...state, theme: payload };
    }
    case types.CHANGE_THEME_FAILURE: {
      return { ...state, error };
    }
    case types.CHANGE_LANGUAGE_SUCCESS: {
      return { ...state, language: payload };
    }
    case types.CHANGE_LANGUAGE_FAILURE: {
      return { ...state, error };
    }
    case types.CHANGE_SOUND_SUCCESS: {
      return { ...state, language: payload };
    }
    case types.CHANGE_SOUND_FAILURE: {
      return { ...state, error };
    }
    default:
      return state;
  }
};

export default layoutReducer;
