import * as types from './actionTypes';

export default language => ({
  type: types.CHANGE_LANGUAGE_REQUEST,
  payload: { language },
});
