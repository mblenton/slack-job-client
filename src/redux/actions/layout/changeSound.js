import * as types from './actionTypes';

export default soundState => ({
  type: types.CHANGE_SOUND_REQUEST,
  payload: { soundState },
});
