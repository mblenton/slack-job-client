import * as types from './actionTypes';

export default theme => ({
  type: types.CHANGE_THEME_REQUEST,
  payload: { theme },
});
