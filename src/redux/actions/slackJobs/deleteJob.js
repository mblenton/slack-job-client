import * as types from './actionTypes';

export default data => ({
  type: types.DELETE_JOB_REQUEST,
  payload: data,
});
