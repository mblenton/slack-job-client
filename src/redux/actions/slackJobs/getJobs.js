import * as types from './actionTypes';

export default () => ({
  type: types.GET_JOBS_REQUEST,
});
