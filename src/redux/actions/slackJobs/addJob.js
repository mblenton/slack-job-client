import * as types from './actionTypes';

export default data => ({
  type: types.ADD_JOB_REQUEST,
  payload: data,
});
