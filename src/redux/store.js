import { createStore, compose, applyMiddleware } from 'redux';
import { createBrowserHistory } from 'history';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from 'redux-saga';

import AppReducer from './reducers';
import Saga from '../saga';

const history = createBrowserHistory();

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, AppReducer);
const sagaMiddleware = createSagaMiddleware();

/* eslint-disable no-underscore-dangle */
export const store = createStore(
  connectRouter(history)(persistedReducer),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  compose(applyMiddleware(
    routerMiddleware(history),
    sagaMiddleware,
  )),
);
export const persistor = persistStore(store);
/* eslint-enable no-underscore-dangle */

sagaMiddleware.run(Saga);
