import React from 'react';
import { Switch, Route } from 'react-router-dom';

import NoRoute from '../components/layout/NoRoute';
import Home from '../components/layout/Home';
import PremiumContent from '../components/PremiumContent';
import SlackJobs from '../components/slackJobs';

import restricted from '../containers/Restricted';

export default () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route exact path="/slackjobs" component={SlackJobs} />
    <Route exact path="/premium" component={restricted(PremiumContent)} />
    <Route component={NoRoute} />
  </Switch>
);
