import { put } from 'redux-saga/effects';

import { CHANGE_LANGUAGE_SUCCESS, CHANGE_LANGUAGE_FAILURE } from '../../redux/actions/layout/actionTypes';

export default function* changeLanguageGen({ payload: { language } }) {
  try {
    yield put({ type: CHANGE_LANGUAGE_SUCCESS, payload: language.itemkey });
  } catch (error) {
    yield put({ type: CHANGE_LANGUAGE_FAILURE, error });
  }
}
