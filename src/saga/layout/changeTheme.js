import { put } from 'redux-saga/effects';

import { CHANGE_THEME_SUCCESS, CHANGE_THEME_FAILURE } from '../../redux/actions/layout/actionTypes';

export default function* changeThemeGen({ payload: { theme } }) {
  try {
    yield put({ type: CHANGE_THEME_SUCCESS, payload: theme.itemkey });
  } catch (error) {
    yield put({ type: CHANGE_THEME_FAILURE, error });
  }
}
