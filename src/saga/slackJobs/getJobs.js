import { call, put } from 'redux-saga/effects';

import { GET_JOBS_SUCCESS, GET_JOBS_FAILURE } from '../../redux/actions/slackJobs/actionTypes';
import getJobs from '../../remoting/slackJobs/getJobs';

export default function* getJobsGen() {
  try {
    const data = yield call(getJobs);
    yield put({ type: GET_JOBS_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: GET_JOBS_FAILURE, error: error.toString() });
  }
}
