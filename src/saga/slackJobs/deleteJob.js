import { call, put } from 'redux-saga/effects';

import { DELETE_JOB_SUCCESS, DELETE_JOB_FAILURE } from '../../redux/actions/slackJobs/actionTypes';
import deleteJob from '../../remoting/slackJobs/deleteJob';

export default function* deleteJobGen({ payload }) {
  try {
    const data = yield call(deleteJob, payload);
    yield put({ type: DELETE_JOB_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: DELETE_JOB_FAILURE, error: error.toString() });
  }
}
