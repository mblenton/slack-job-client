import { call, put } from 'redux-saga/effects';

import { ADD_JOB_SUCCESS, ADD_JOB_FAILURE } from '../../redux/actions/slackJobs/actionTypes';
import addJob from '../../remoting/slackJobs/addJob';

export default function* getJobsGen({ payload }) {
  try {
    const data = yield call(addJob, payload);
    yield put({ type: ADD_JOB_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: ADD_JOB_FAILURE, error: error.toString() });
  }
}
