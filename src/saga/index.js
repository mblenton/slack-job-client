import { takeLatest } from 'redux-saga/effects';

import { GET_JOBS_REQUEST, ADD_JOB_REQUEST, DELETE_JOB_REQUEST } from '../redux/actions/slackJobs/actionTypes';
import getJobsGen from './slackJobs/getJobs';
import addJobGen from './slackJobs/addJob';
import deleteJob from './slackJobs/deleteJob';

import { CHANGE_THEME_REQUEST, CHANGE_LANGUAGE_REQUEST } from '../redux/actions/layout/actionTypes';
import changeThemeGen from './layout/changeTheme';
import changeLanguageGen from './layout/changeLanguage';

function* Saga() {
  yield takeLatest(GET_JOBS_REQUEST, getJobsGen);
  yield takeLatest(ADD_JOB_REQUEST, addJobGen);
  yield takeLatest(DELETE_JOB_REQUEST, deleteJob);
  yield takeLatest(CHANGE_THEME_REQUEST, changeThemeGen);
  yield takeLatest(CHANGE_LANGUAGE_REQUEST, changeLanguageGen);
}

export default Saga;
