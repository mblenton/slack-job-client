import localeEn from 'react-intl/locale-data/en';
import localeDe from 'react-intl/locale-data/de';
import localeHr from 'react-intl/locale-data/hr';

import messagesDe from './de.json';
import messagesEn from './en.json';
import messagesHr from './hr.json';

export const translatedMessages = {
  de: messagesDe,
  en: messagesEn,
  hr: messagesHr,
};

const languageFromBrowser =
  (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
export const languageFromBrowserWithoutRegionCode = languageFromBrowser.toLowerCase().split(/[_-]+/)[0];

export const localeData = [...localeEn, ...localeDe, ...localeHr];

const tmp = {
  translations: [
    { name: 'English', key: 'en' },
    { name: 'Deutch', key: 'de' },
    { name: 'Hrvatski', key: 'hr' },
  ],
};

export default tmp.translations;
